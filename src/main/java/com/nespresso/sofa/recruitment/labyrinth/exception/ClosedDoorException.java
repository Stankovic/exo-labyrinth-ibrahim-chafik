package com.nespresso.sofa.recruitment.labyrinth.exception;

public class ClosedDoorException extends RuntimeException {

	public ClosedDoorException() {
		super();
	}
}
