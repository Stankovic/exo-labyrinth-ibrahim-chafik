package com.nespresso.sofa.recruitment.labyrinth.exception;

public class DoorAlreadyClosedException extends RuntimeException {

	public DoorAlreadyClosedException() {
		super();
	}
}
