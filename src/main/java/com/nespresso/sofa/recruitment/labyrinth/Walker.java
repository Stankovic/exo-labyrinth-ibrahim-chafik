package com.nespresso.sofa.recruitment.labyrinth;

import com.nespresso.sofa.recruitment.labyrinth.exception.ClosedDoorException;
import com.nespresso.sofa.recruitment.labyrinth.exception.DoorAlreadyClosedException;
import com.nespresso.sofa.recruitment.labyrinth.exception.IllegalMoveException;

public class Walker {

	private String previousPisiton;
	private String currentPosition;
	private Labyrinth labyrinth;

	public Walker(String position, Labyrinth labyrinth) {
		this.currentPosition = position;
		this.labyrinth = labyrinth;
	}

	public void walkTo(String nextPosition) {
		if (this.canWalkTo(nextPosition)) {
			this.previousPisiton = currentPosition;
			this.currentPosition = nextPosition;
			labyrinth.updateSensors(previousPisiton, currentPosition);
		} else {
			throw new IllegalMoveException();
		}
	}

	public String position() {
		return this.currentPosition;
	}

	public void closeLastDoor() {
		String doorToClose = previousPisiton.concat(currentPosition);
		if (labyrinth.getClosedDoor().isEmpty()) {
			labyrinth.setClosedDoor(doorToClose);
		} else {
			if (doorToClose.equals(labyrinth.getClosedDoor())) {
				throw new DoorAlreadyClosedException();
			}
		}
	}

	private boolean canWalkTo(String nextPosition) {
		if (labyrinth.isDoorClosed(currentPosition, nextPosition)) {
			throw new ClosedDoorException();
		} else {
			if (canGoBackward(nextPosition) || canGoForward(nextPosition)) {
				return true;
			}
		}
		return false;
	}

	private boolean canGoBackward(String nextPosition) {
		for (Step step : labyrinth.getSteps()) {
			if (nextPosition.equals(step.getFromPosition())
					&& currentPosition.equals(step.getToPosition())) {
				return true;
			}
		}
		return false;
	}

	private boolean canGoForward(String nextPosition) {
		for (Step step : labyrinth.getSteps()) {
			if (currentPosition.equals(step.getFromPosition())
					&& nextPosition.equals(step.getToPosition())) {
				return true;
			}
		}
		return false;
	}
}
