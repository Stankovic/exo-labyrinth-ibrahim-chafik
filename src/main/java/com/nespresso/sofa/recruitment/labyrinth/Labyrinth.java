package com.nespresso.sofa.recruitment.labyrinth;

import java.util.ArrayList;
import java.util.List;

public class Labyrinth {

	private List<Step> steps;
	private StringBuilder sensors;
	public String closedDoor;
	private static final int FROM_PIECE_POSITION = 0;
	private static final int DOOR_POSITION = 1;
	private static final int TO_PIECE_POSITION = 2;
	private static final String SENSOR_SEPARATOR = ";";

	public Labyrinth(String... inputSteps) {
		this.steps = new ArrayList<>();
		this.sensors = new StringBuilder();
		this.closedDoor = "";
		for (String step : inputSteps) {
			Step newStep = new Step(step.charAt(FROM_PIECE_POSITION),
					step.charAt(TO_PIECE_POSITION), step.charAt(DOOR_POSITION));
			steps.add(newStep);
		}
	}

	public Walker popIn(String position) {
		Step firstStep = getFirstStepStartWithPosition(position);
		return new Walker(firstStep.getFromPosition(), this);
	}

	public String getDoorTypeBetween(String fromPosition, String toPosition) {
		for (Step step : this.steps) {
			if (step.getFromPosition().equals(fromPosition)
					&& step.getFromPosition().equals(toPosition)) {
				return step.getDoor();
			}
		}
		return null;
	}

	public void addSensor(String sensor) {
		if (sensors.length() == 0) {
			sensors.append(sensor);
		} else {
			sensors.append(SENSOR_SEPARATOR).append(sensor);
		}
	}

	public Step getStepUsingCyclicSearch(String fromPosition, String toPosition) {
		for (Step step : this.steps) {
			if (step.getFromPosition().equals(fromPosition)
					&& step.getToPosition().equals(toPosition)) {
				return step;
			} else if (step.getFromPosition().equals(toPosition)
					&& step.getToPosition().equals(fromPosition)) {
				return step;
			}
		}
		return null;
	}

	public boolean isDoorClosed(String from, String to) {
		if (closedDoor.isEmpty()) {
			return false;
		}
		String forwardDoor = from.concat(to);
		String backwardDoor = to.concat(from);
		if (closedDoor.equals(forwardDoor) || closedDoor.equals(backwardDoor)) {
			return true;
		}
		return false;
	}

	public void updateSensors(String previousPisiton, String currentPosition) {
		Step currentStep = this.getStepUsingCyclicSearch(previousPisiton,
				currentPosition);
		if (currentStep.hasSensor()) {
			this.addSensor(currentStep.getSensor());
		}
	}

	public String getClosedDoor() {
		return closedDoor;
	}

	public void setClosedDoor(String closedDoor) {
		this.closedDoor = closedDoor;
	}

	public String readSensors() {
		return this.sensors.toString();
	}

	public List<Step> getSteps() {
		return this.steps;
	}

	public StringBuilder getSensors() {
		return this.sensors;
	}

	private Step getFirstStepStartWithPosition(String position) {
		for (Step step : steps) {
			if (step.getFromPosition().equals(position)) {
				return step;
			}
		}
		return null;
	}
}
