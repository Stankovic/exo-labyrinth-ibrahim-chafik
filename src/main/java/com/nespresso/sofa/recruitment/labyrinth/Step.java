package com.nespresso.sofa.recruitment.labyrinth;

public class Step {
	private String fromPosition;
	private String toPosition;
	private String door;

	private static final String SENSOR_DOOR = "$";

	public Step(Character from, Character to, Character door) {
		this.fromPosition = from.toString();
		this.toPosition = to.toString();
		this.door = door.toString();
	}

	public boolean hasSensor() {
		if (SENSOR_DOOR.equals(this.door)) {
			return true;
		}
		return false;
	}

	public String getSensor() {
		return fromPosition.concat(toPosition);
	}

	public String getDoor() {
		return door;
	}

	public String getFromPosition() {
		return fromPosition;
	}

	public String getToPosition() {
		return toPosition;
	}

}
